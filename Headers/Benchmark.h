/*
 * Benchmark.h
 *
 *  Created on: 31 Oct 2016
 *      Author: USER
 */

#ifndef SOURCES_BENCHMARK_H_
#define SOURCES_BENCHMARK_H_

void init_timer();
void reset();
double get_time_delta();
double getTimeDeltaFromStart();
long long getTimeDeltaNs();

#endif
