#ifndef HEADERS_MEM_MANAGE_H_
#define HEADERS_MEM_MANAGE_H_

void* init(long size);

void release_mem();

void* release(void* v);
#endif
