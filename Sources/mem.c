#include <mem_manage.h>
#include <stdlib.h>

static void** allocated = 0;
static unsigned long slots = 0;

/**
 * This function gets a slot of a pointer inside the allocated pool.
 * @param pointer the pointer to search in the pool.
 */
static int get_slot(void* pointer) {
	for (int slot = 0; slot < slots; slot++)
		if (allocated[slot] == pointer)
			return slot;
	return -1;
}

/**
 * This function allocates memory of size and puts it in the managed pool.
 * @param size the size to allocate.
 * @return the allocated pointer.
 */
void* init(long size) {
	//Allocate memomry of size.
	int* ptr = malloc(size);
	//Clear all memory.
	for (int offset = 0; offset < size / sizeof(int); offset++)
		ptr[offset] = 0;
	//If the pool does not exist allocate it, else reallocate it to a bigger size.
	if (allocated == 0)
		allocated = malloc(++slots * sizeof(void*));
	else
		allocated = realloc(allocated, ++slots * sizeof(void*));
	//Put in the managed pool.
	allocated[slots - 1] = ptr;
	return ptr;
}

/**
 * This function expends a pointer's domain.
 * @param pointer the pointer to expend.
 * @param the new size for the pointer.
 * @return the new pointer.
 */
void* expend(void* pointer, size_t new_size) {
	// Get the pointer's slot.
	int slot = get_slot(pointer);
	//If the pointer was found.
	if (slot != -1) {
		//reallocate this pointer to have a wider area.
		allocated[slot] = realloc(allocated[slot], new_size);
		//TODO: improve...
		return allocated[slot];
	} else
		return 0;
}
/**
 * This function releases all memory that wasn't already freed from the managed pool.
 */
void release_mem() {
	//Free all pointers.
	for (int slot = 0; slot < slots; slot++)
		free(allocated[slot]);
	//Free the pool pointer.
	free(allocated);
	allocated = 0;
}

/**
 * This function releases one pointer.
 * @param pointer the pointer to free.
 * @return NULL
 */
void* release(void* pointer) {
	int slot = get_slot(pointer);
	if (slot == -1)
		return 0;
	//Free pointer.
	free(pointer);
	//Copy all elements after this pointer.
	for (int var = slot; var < slots - 1; var++)
		allocated[var] = allocated[var + 1];
	//Resize managed pool.
	allocated = realloc(allocated, --slots * sizeof(void*));
	return 0;
}
