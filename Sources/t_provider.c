#include <time.h>
//#ifdef  __has_include
//#if __has_include("windows.h")
#ifdef WIN32
#include <windows.h>
#include <stdio.h>
// Windows implementation of clock_gettime by GetSystemTime.
/**
 * This function gets the current os time.
 * @oaram clock_id not used in this implementation.
 * @param t the time structure to update.
 * @return NUll.
 */
int clock_gettime(clockid_t clock_id, struct timespec *t){
	SYSTEMTIME time;
	GetSystemTime(&time);
	t->tv_sec = time.wSecond;
	t->tv_nsec = time.wMilliseconds * 1000000;
	return 0;
}
#else
static double freq;
static long freqs;
static LARGE_INTEGER start;
//PerformanceCounter implementation for old builds.
/**
 * This function gets the current os time.
 * @oaram clock_id not used in this implementation.
 * @param t the time structure to update.
 * @return NUll.
 */
int clock_gettime(clockid_t clock_id, struct timespec *t) {
	if (!freq) {
		LARGE_INTEGER freq_query;
		if (QueryPerformanceFrequency(&freq_query)) {
			QueryPerformanceCounter(&start);
			freq = 1000000000.0 / freq_query.QuadPart;
			freqs = freq_query.QuadPart;
		} else
			freq = 0;
	}
	if (!freq)
		return -1;
	else {
		LARGE_INTEGER time;
		if (QueryPerformanceCounter(&time)) {
			t->tv_nsec = (long long) (time.QuadPart * freq) % 1000000000;
//			t->tv_sec = (time.QuadPart% (60*freqs)) / freqs ;
			t->tv_sec = (time.QuadPart / freqs -12) % 60;
		} else
			return -1;
	}
	return 1;
}
#endif
