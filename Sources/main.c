#include <Benchmark.h>
#include <stdio.h>
#include <Head.h>
#include <time.h>
#include <Windows.h>
#include <mem_manage.h>

//X2.5 faster then t a(non pointer)
void proccess(t* a) {
	a->area = a->movea;
}

int main() {
	//Initialize the timer.
	init_timer();
	//Loops.
	//X8 faster then int i = 0; ...
	register unsigned int i = 0;
	for (i = 0; i < 1000000000; ++i) { }
	//Print time delta and reset timer.
	printf("%f\n", get_time_delta());
	reset();

	//Functions(pointer function).
	i = 0;
	t s;
	for (i = 0; i < 1000000000; ++i) {
		proccess(&s);
	}
	printf("%f\n", get_time_delta());
	reset();

	//Memory allocation and management.
	FILETIME* time = init(sizeof(FILETIME));
	FILETIME* spec = init(sizeof(FILETIME));
	printf("%lu\n", spec->dwHighDateTime);
	printf("%lu\n", time->dwHighDateTime);
	time = release(time);
	struct timespec t;
	printf("%f\n", get_time_delta());
	reset();

	//Clock query.
	clock_gettime(CLOCK_MONOTONIC, &t);
	printf("%ld", t.tv_sec);
	printf("%f\n", get_time_delta());
	reset();

	//Cleanup.
	release_mem();
	//PrintTime in nanoseconds.
	printf("%I64d", getTimeDeltaNs());
//Don't pass more then 4 args to func it turn's into stack memory access mess...
//use 'leaf functions' and inlines for frequant called funcitons
}

//Vector maths.
//typedef char v16qi __attribute__ ((vector_size (16)));
//void swap3(char *orig, char *dest, size_t imagesize) {
//    v16qi mask = __builtin_ia32_lddqu((const char[]){3,2,1,7,6,5,11,10,9,15,14,13,0xFF,0xFF,0xFF,0XFF});
//    char *end = orig + imagesize * 4;
//    for (; orig != end; orig += 16, dest += 12) {
//        __builtin_ia32_storedqu(dest,__builtin_ia32_pshufb128(__builtin_ia32_lddqu(orig),mask));
//    }
//}
