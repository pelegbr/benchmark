#include <Benchmark.h>
#include <windows.h>
//The time the timer was initialized.
static LARGE_INTEGER Start;
//The last time the timer was started.
static LARGE_INTEGER last;
//The frequency of this cpu.
static double freq;
/**
 * This function initializes the timer for the first time.
 */
void init_timer() {
	//Query frequency.
	if (QueryPerformanceFrequency(&Start))
		freq = ((double) Start.QuadPart) / 1000.0;
	else
		freq = 0.0;
	//Query start time.
	QueryPerformanceCounter(&Start);
	last = Start;
}

/**
 * This function restarts the timer.
 */
void reset() {
	QueryPerformanceCounter(&last);
}

/**
 * This function gets the time since restart/get_time_* was called.
 */
double get_time_delta() {
	LONG64 start = last.QuadPart;
	if (QueryPerformanceCounter(&last) && freq)
		return ((double) last.QuadPart - start) / freq;
	return -1.0;
}

/**
 * This function gets the time since init_timer was called.
 */
double getTimeDeltaFromStart() {
	LONG64 start_time = Start.QuadPart;
	if (QueryPerformanceCounter(&last) && freq)
		return ((double) last.QuadPart - start_time) / freq;
	return -1.0;
}

/**
 * This function gets the time since restart/get_time_* was called in nanoseconds.
 */
LONG64 getTimeDeltaNs() {
	LONG64 start = last.QuadPart;
	if (QueryPerformanceCounter(&last) && freq)
		return (((double) last.QuadPart - start) / freq) * 1000000;
	return -1;
}
